# GH - An app that make game hints from some famous games. Also this my very
# first app.


# Copyright 2020 Ben Rogé

# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
# SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
# RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
# CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import random
print("what game do you want to learn about? minecraft, pubg")
answer = input(",Roblox jailbreak or Ninja Legends (on Roblox)")
answer = answer.lower()

if answer == "minecraft":
    hint_number = random.randrange(4)
    if hint_number == 0:
        print("In mincraft do not dig strait down or you might")
        print("die from lava and you cant save yourself")

    if hint_number == 1:
        print("Minecraft is a fun game where you try to survive to beat the")
        print("ender dragon to beat the game.")
    if hint_number == 2:
        print("if you beat te ender dragon you can summon it back to")
        print("life at where you teleport to the overworld.")
    if hint_number == 3:
        print("In the new nether update you might want to dig with bed")
        print("in the nether to find the block for THE NEW")
        print("BEST ARMOR IN THE GAME, but the only downside is that it is")
        print("dangerus.")

if answer == "pubg":
    hint_number = random.range(2)
    if hint_number == 0:
        print("In pubg when you are jumping out the plane")
        print("at the begining of the game")
        print("if you are trying to go a far dictance point")
        print("strait up and go fowards")
        print("but when you are going a short dictance piont")
        print("straight down and sideways")
        print("and then you will be there first.")
    if hint_number == 1:
        print("In Pubg if you are fighting close range scope in it")
        print("will do more damage then not scoping")

# if answer == "fortnite":
#     print("In fortnite ")

if answer == "roblox jailbreak":
    hint_number = random.randrange(2)
    if hint_number == 0:
        print("In roblox jailbreak if you are a prisoner take")
        print("the sewer it is more risky but")
        print("the quickest way out.")
    if hint_number == 1:
        print("If you are going to rob the museum always bring a freiend")
        print("and leave a car outside to go to the bad bace to get money.")
if answer == "ninja legends":
    hint_number = random.randrange(4)
    if hint_number == 0:
        print("In Ninja Legends you train to be the best ninja of all time.")
    if hint_number == 1:
        print("Before going to the higher islands, get a better")
        print("rank because you will")
        print("earn much more gems,karma and souls from the chests.")
    if hint_number == 2:
        print("the gold island in Ninja legends gives much more chi than all")
        print("the other islands.")
    if hint_number == 3:
        print("At the top islands there is sell spots that have a sell multiplyer")