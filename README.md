# GH

An app that make game hints from some famous games. Also this my very first app.

## Credits
Game Hints was created by Ben Rogé. The Game Hints website was created by Ben Rogé and modified by Jason Yundt. The file .gitlab-ci.yml was created by Jason Yundt using the ["Create a GitLab Pages website from scratch"](https://gitlab.com/gitlab-org/gitlab/blob/master/doc/user/project/pages/getting_started_part_four.md) tutorial by [GitLab](https://gitlab.com/gitlab-org), [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
